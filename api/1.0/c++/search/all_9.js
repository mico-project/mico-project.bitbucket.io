var searchData=
[
  ['languageliteral',['LanguageLiteral',['../classmico_1_1rdf_1_1model_1_1LanguageLiteral.html',1,'mico::rdf::model']]],
  ['literal',['Literal',['../classmico_1_1rdf_1_1model_1_1Literal.html',1,'mico::rdf::model']]],
  ['load',['load',['../classmico_1_1persistence_1_1Metadata.html#a417801f22c8eb4584e5b6e16f870c1da',1,'mico::persistence::Metadata']]],
  ['loadfrom',['loadFrom',['../classmico_1_1rdf_1_1query_1_1QueryResult.html#a895b4db91a5ffe34f98f5e3e6cf5a1a8',1,'mico::rdf::query::QueryResult::loadFrom(std::istream &amp;is)=0'],['../classmico_1_1rdf_1_1query_1_1QueryResult.html#aa133ed731b09922ecf89f5c17c921497',1,'mico::rdf::query::QueryResult::loadFrom(const char *ptr, size_t len)=0'],['../classmico_1_1rdf_1_1query_1_1BooleanResult.html#aa6f29fe0994b1a2aad47731dddfd8f0b',1,'mico::rdf::query::BooleanResult::loadFrom(std::istream &amp;is)'],['../classmico_1_1rdf_1_1query_1_1BooleanResult.html#a9b62fc799a7ebd712891b8185f0c5bb2',1,'mico::rdf::query::BooleanResult::loadFrom(const char *ptr, size_t len)'],['../classmico_1_1rdf_1_1query_1_1TupleResult.html#afe97f71186f46674b8de211de2f115b1',1,'mico::rdf::query::TupleResult::loadFrom(std::istream &amp;is)'],['../classmico_1_1rdf_1_1query_1_1TupleResult.html#a40f996fe5557cf783d38f74493bf5a92',1,'mico::rdf::query::TupleResult::loadFrom(const char *ptr, size_t len)']]],
  ['longvalue',['longValue',['../classmico_1_1rdf_1_1model_1_1Literal.html#ac713d45beccceff5a47bc9a0e923a48d',1,'mico::rdf::model::Literal']]],
  ['loop',['loop',['../classmico_1_1io_1_1URLStreambufBase.html#ac6a9a6c312417e488269f0565dda2a2a',1,'mico::io::URLStreambufBase']]]
];
